<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rogue
 *
 * @author pabhoz
 */
class Rogue extends CharacterFactory{
    //put your code here
    private $nombre, $raza,$hp, $mn, $str, $md, $ag;
    private $clase= "rogue";
    
    function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag, $clase) {
        
        parent::__construct($nombre, $raza,$hp, $mn, $str, $md, $ag, $this->clase);
        $this->setHp($this->getHp()*1.2); //esto es incorrecto e inadecuado
        $this->setMn($this->getMn()*0);
        $this->setAg($this->getAg()*2);
    }
}
