<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Character
 *
 * @author pabhoz
 */
class Character {
    //put your code here
    private $nombre, $raza,$hp, $mn, $str, $md, $ag;
    private $clase= "normal";
   
    function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag){
        $this->nombre = $nombre;
        $this->raza = $raza;
        $this->hp = $hp;
        $this->mn = $mn;
        $this->str = $str;
        $this->md = $md;
        $this->ag = $ag;    
    }
    
    function getNombre() {
        return $this->nombre;
    }
    
    function getRaza() {
        return $this->raza;
    }
    
    function getHp() {
        return $this->hp;
    }
    
    function getMn() {
        return $this->mn;
    }
    
    function getStr() {
        return $this->str;
    }

    function getMd() {
        return $this->md;
    }
    
    function getAg() {
        return $this->ag;
    }
    
    function getClase() {
        return $this->clase;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    function setRaza($raza) {
        $this->raza = $raza;
    }
    
    function setHp($hp) {
        $this->hp = $hp;
    }
    
    function setMn($mn) {
        $this->mn = $mn;
    }
    
    function setStr($str) {
        $this->str = $str;
    }
    
    function setMd($md) {
        $this->md = $md;
    }
    
    function setAg($ag) {
        $this->ag = $ag;
    }
    
    function setClase($clase) {
        $this->clase = $clase;
    }
    
    private function calculaMayorAtributo(){
        
        if($this->getStr()>$this->getMd()){
            if($this->getSrd()>$this->getAg()){
                return $this->getStr();
            }
        }
        else{
            if($this->getMd()>$this->getAg()){
                return $this->getMd();
            }
            else{
                return $this->getAg();
            }
        }
    }
    public function attack(){ //El método debe recibir una victima
        
        $atributoMayor= $this->calculaMayorAtributo(); //era más fácil con un max()
        
        
        if(strcmp ( $this->getClase() , "normal" )){ //Y esto para qué?
            
            $daño= $atributoMayor*0.4;
            
            return $this->getNombre()." ataca a ";
            
        }
        
        //eturn $this->
    } 
    
    public function getHurt($hp){
        
        $this->getHp()=$this->getHp()-$hp;// error, no solo en la implementación sino también en la sintaxis.
        if($this->getHp()<=0){
            $this->dramaticDeath();
        }
        
    }
    
    public function dramaticDeath(){
        
        return "Arrrrg! yo ". $this->getNombre(). " he sido derrotado en batalla";
        
    }
   
}