#no se define la línea del compilador !#/bin/bash
sudo wget https://www.apachefriends.org/xampp-files/7.1.14/xampp-linux-x64-7.1.14-0-installer.run; #sin el password no ejecutará automáticamente a menos que la cuenta no tengo seguridad o esté deshabilitada
sudo chmod 777 xampp-linux-x64-7.1.14-0-installer.run; # igual acá, podrías haber usado echo "password" | sudo -S comando;
sudo ./xampp-linux-x64-7.1.14-0-installer.run; # lo mismo acá, de hecho el instalador no terminará de instalar

alias lampp="sudo /opt/lampp/lampp"; #la definición del alias no es necesaria pero bueno...

lampp start; # la idea es que esto sucediera en cada inicio de sesión y acá no sucederá así
